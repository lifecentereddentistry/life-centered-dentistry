LCD and Dr. John Ludwig have been serving the dental needs of families in West Michigan for over 37 years. At LCD, you are involved in your care. It is customized to reflect your dental health goals and how to best achieve them.

Address: 2030 Leonard St NW, Grand Rapids, MI 49504, USA

Phone: 616-453-3111

Website: https://lifecentereddentistry.com
